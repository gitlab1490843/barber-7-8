document.addEventListener('DOMContentLoaded', function () {
    var centerColumn = document.querySelector('.center-column');
    var centerRightColumn = document.querySelector('.center-right-column');
    var leftColumn = document.querySelector('.left-column');
    var rightColumn = document.querySelector('.right-column');

    centerColumn.addEventListener('animationend', function () {
        setTimeout(function () {
            leftColumn.classList.add('moveLeftColumn');
        }, 80);
    });

    centerRightColumn.addEventListener('animationend', function () {
        setTimeout(function () {
            rightColumn.classList.add('moveRightColumn');
        }, 80);
    });
});

document.addEventListener('DOMContentLoaded', function () {
    const cards = document.querySelectorAll('.card');

    cards.forEach(card => {
        card.addEventListener('mouseenter', () => {
            cards.forEach(otherCard => {
                if (otherCard !== card) {
                    otherCard.classList.add('darken');
                }
            });
        });

        card.addEventListener('mouseleave', () => {
            cards.forEach(otherCard => {
                if (otherCard !== card) {
                    otherCard.classList.remove('darken');
                }
            });
        });
    });
});